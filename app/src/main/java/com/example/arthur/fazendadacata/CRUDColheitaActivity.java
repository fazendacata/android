package com.example.arthur.fazendadacata;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CRUDColheitaActivity extends AppCompatActivity {

    static final int DATE_DIALOG_ID = 999;
    private TextView dataColheita;
    private TextView dataEntradaTerreiro;
    private TextView dataEntradaSecador;
    private TextView dataSaidaTerreiro;
    private TextView dataSaidaSecador;
    private CheckBox checkBoxTerreiro;
    private CheckBox checkBoxSecador;
    private int year;
    private int month;
    private int day;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crudcolheita);
        setCurrentDateOnDataColheita();
        addListenerOnData();
        checkBoxTerreiro = (CheckBox) findViewById(R.id.checkBoxTerreiro);
        checkBoxTerreiro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                        @Override
                                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                            LinearLayout terreiro = (LinearLayout) findViewById(R.id.linearLayoutTerreiro);
                                                            ViewGroup.LayoutParams lp = terreiro.getLayoutParams();
                                                            if (isChecked) {
                                                                lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                                                setCurrentDateOnDataTerreiro();
                                                            } else {
                                                                lp.height = 0;
                                                            }
                                                            terreiro.requestLayout();
                                                        }
                                                    }
        );
        checkBoxSecador = (CheckBox) findViewById(R.id.checkBoxSecador);
        checkBoxSecador.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                       @Override
                                                       public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                           LinearLayout secador = (LinearLayout) findViewById(R.id.linearLayoutSecador);
                                                           ViewGroup.LayoutParams lp = secador.getLayoutParams();
                                                           if (isChecked) {
                                                               lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                                               setCurrentDateOnDataSecador();
                                                           } else {
                                                               lp.height = 0;
                                                           }
                                                           secador.requestLayout();
                                                       }
                                                   }
        );
    }

    // display current date
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setCurrentDateOnDataColheita() {

        dataColheita = (TextView) findViewById(R.id.editTextData);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview

        dataColheita.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(day).append("/")
                .append(month + 1).append("/")
                .append(year));
    }

    // display current date
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setCurrentDateOnDataTerreiro() {
        dataEntradaTerreiro = (TextView) findViewById(R.id.editTextDataEntradaTerreiro);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        dataEntradaTerreiro.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(day).append("/")
                .append(month + 1).append("/")
                .append(year));
    }

    // display current date
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setCurrentDateOnDataSecador() {
        dataEntradaSecador = (TextView) findViewById(R.id.editTextDataEntradaSecador);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        dataEntradaSecador.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(day).append("/")
                .append(month + 1).append("/")
                .append(year));
    }

    public void addListenerOnData() {

        dataColheita = (EditText) findViewById(R.id.editTextData);
        dataEntradaTerreiro = (EditText) findViewById(R.id.editTextDataEntradaTerreiro);
        dataEntradaSecador = (EditText) findViewById(R.id.editTextDataEntradaSecador);
        dataSaidaTerreiro = (EditText) findViewById(R.id.editTextDataSaidaTerreiro);
        dataSaidaSecador = (EditText) findViewById(R.id.editTextDataSaidaSecador);

        dataColheita.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });

        dataEntradaTerreiro.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });

        dataEntradaSecador.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });
        dataSaidaTerreiro.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });
        dataSaidaSecador.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview
            dataColheita.setText(new StringBuilder().append(day).append("/").append(month + 1)
                    .append("/").append(year));

        }
    };
}
